package com.chonwhite.core;

import android.os.Handler;
import android.os.Looper;

import java.util.ArrayList;

public class ModelManager <K,E>{
	
	private static Handler sHandler = new Handler(Looper.getMainLooper());
	
	private ArrayList<ModelStatusListener<K, E>> listeners = new ArrayList<>() ;
	
	public void registerListener(ModelStatusListener<K, E> l){
		listeners.add(l);
	}
	
	public void unregisterListener(ModelStatusListener<K, E> l){
		listeners.remove(l);
	}
	
	public ArrayList<ModelStatusListener<K, E>> getListener(){
		return listeners;
	}
	
	public void notifyEvent(final K event,final E model){
		if(Thread.currentThread() == Looper.getMainLooper().getThread()){
			for(ModelStatusListener<K, E> l:listeners){
				l.onModelEvent(event, model);
			}
		}else{
			sHandler.post(new Runnable() {
				@Override
				public void run() {
					for(ModelStatusListener<K, E> l:listeners){
						l.onModelEvent(event, model);
					}
				}
			});
		}
	}
}
