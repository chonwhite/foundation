package com.chonwhite.core;

public enum ModelEvent {
	OnModelGet,
	OnModelUpdate,
	OnModelsGet,
	OnModelGetFailed,
}
