package com.chonwhite.core;

public interface ModelStatusListener<K,E> {
	void onModelEvent(K event, E model);
}
