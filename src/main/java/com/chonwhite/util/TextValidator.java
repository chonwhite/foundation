package com.chonwhite.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextValidator {
    static Pattern mobileNumberPattern = Pattern
            .compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");

    public static boolean isMobileNumber(String text) {
        Matcher m = mobileNumberPattern.matcher(text);
        return m.matches();
    }

    public static boolean isId(String text){
        return text.length() == 18;//TODO
    }

    private static Pattern p = Pattern.compile("/(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)/");
    public static boolean isID(String text){
        return p.matcher(text).matches();
    }
}
