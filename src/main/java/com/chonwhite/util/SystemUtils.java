package com.chonwhite.util;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.chonwhite.core.ContextProvider;

/**
 * Created by txy on 15/8/30.
 */

public class SystemUtils {

    public static String getVersionName() {
        PackageManager pm = ContextProvider.getContext().getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(ContextProvider.getContext().getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }
}
