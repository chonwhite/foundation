package com.chonwhite.json;

import android.util.Log;

import com.chonwhite.http.HttpEvent;
import com.chonwhite.http.HttpManager;
import com.chonwhite.http.HttpRequest;
import com.chonwhite.http.ListResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONListParser implements HttpRequest.Parser<ListResult<JSONModel>, String> {

    public boolean isPaged() {
        return paged;
    }

    public void setPaged(boolean paged) {
        this.paged = paged;
    }

    private boolean paged = true;
    /**
     当result的数组参数 不是pageItems 而是list
     */
    private boolean isList = false;
    public void setIsList(boolean isList){
        this.isList = isList;
    }


    @Override
    public ListResult<JSONModel> parse(String is) {
        ListResult<JSONModel> mr = new ListResult<>();
        Log.e("xx", "32---------is=" + is);
        try {
            JSONObject rootObj = new JSONObject(is);

            int statusCode = rootObj.optInt("code", -1);

            if (statusCode >= 200 & statusCode < 300) {
                JSONObject result = null;
                JSONArray array = null;
                if(isPaged()){
                    array = rootObj.optJSONArray("data");
//                    result = rootObj.optJSONObject("data");
//                    if(result != null) {
//                        if(isList){
//                            array = result.optJSONArray("list");
//                        }else{
//                            array = result.optJSONArray("pageItems");
//                        }
//                    }
                }else{
                    array = rootObj.optJSONArray("data");
                }

                if(array != null){
                    ArrayList<JSONModel> models = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject je = array.optJSONObject(i);
                        JSONModel m = new JSONModel();
                        JSONModelMapper.mapObject(je, m);
                        models.add(m);
                    }

                    ListResult.Page page = new ListResult.Page();
                    result = rootObj.optJSONObject("page");
                    if(result != null && paged){
                        page.setPageIndex(result.optInt("current"));
                        page.setTotalPageNumber(result.optInt("pages"));
                        page.setLastPage(page.getTotalPageNumber()==0 || page.getPageIndex() == page.getTotalPageNumber());
                    }else{
                        page.setPageIndex(1);
                        page.setTotalPageNumber(1);
                        page.setLastPage(true);
                    }
                    mr.setPage(page);
                    mr.setModels(models);
                }
            } else {
                mr.setCode(statusCode);
                mr.setDesc(rootObj.getString("msg"));
                if (statusCode == 911 || statusCode == 401) {
                    HttpManager.getInstance().notifyEvent(HttpEvent.accessTokenExpired, mr);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mr;
    }
}
