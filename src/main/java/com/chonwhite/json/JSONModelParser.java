package com.chonwhite.json;


import android.util.Log;

import com.chonwhite.http.HttpRequest;
import com.chonwhite.http.parser.ModelResult;

import org.json.JSONException;
import org.json.JSONObject;

public class JSONModelParser implements HttpRequest.Parser<ModelResult<JSONModel>, String> {
    @Override
    public ModelResult<JSONModel> parse(String s) {
        ModelResult<JSONModel> result = new ModelResult<>();
        Log.e("xx", "s:" + s);
        result.setCode(-1);
        try {
            JSONObject rootObj = new JSONObject(s);
            result.setCode(200);
            JSONObject resultObj = rootObj.optJSONObject("data");
            JSONModel model = new JSONModel();
            JSONModelMapper.mapObject(rootObj, model);
            result.setModel(model);
            //TODO
//            if (statusCode == 911 || statusCode == 401) {
//                HttpManager.getInstance().notifyEvent(HttpEvent.accessTokenExpired, result);
//            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
