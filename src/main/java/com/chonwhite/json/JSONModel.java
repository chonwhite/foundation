package com.chonwhite.json;

import android.text.TextUtils;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private HashMap<String, Object> classMap = new HashMap<>();
    private HashMap<String, List<?>> arrayMap = new HashMap<>();

    public String getString(String key) {
        return get(key, String.class);
    }

    public int getInt(String key) {
        int val = 0;
        Object value = get(key);
        if(value == null){
            return 0;
        }
        if(value instanceof Number){
            Number number = (Number) value;
            return number.intValue();
        }
        if(value instanceof String){
            String stringValue = (String) value;
            try{
                val = Integer.parseInt(stringValue);
            }catch (Exception e){

            }
        }
        return val;
    }

    public double getDouble(String key) {
        Double value = getKeyPath(key, Double.class);
        if (value == null) {
            Integer intValue = get(key, Integer.class);
            if (intValue != null) {
                return intValue;
            }
            String stringValue = get(key, String.class);
            if (stringValue != null && !TextUtils.isEmpty(stringValue)) {
                try {
                    return Double.parseDouble(stringValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return value == null ? 0 : value;
    }

    public Object get(String key) {
        return classMap.get(key);
    }

    public boolean getBoolean(String key) {
        return get(key, Boolean.class);
    }

    public JSONModel getModel(String key) {
        return get(key, JSONModel.class);
    }

    public <T> T getKeyPath(String key, Class<T> clz) {
        String[] keys = key.split("\\.");
        T result = null;
        JSONModel m = this;
        for (int i = 0; i < keys.length - 1; i++) {
            m = m.getModel(keys[i]);
            if (m == null) {
                return null;
            }
        }
        try {
            result = m.get(keys[keys.length - 1], clz);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public <T> T get(String key, Class<T> clz) {
        Object itemMap = classMap.get(key);
        return (T) itemMap;
    }

    public <T> void set(String key, T value) {
        classMap.put(key, value);
    }

    public void setList(String key, List<?> models) {
        arrayMap.put(key, models);
    }

    public <T> List<T> getList(String key, Class<T> clz) {
        return (List<T>) arrayMap.get(key);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(classMap);
        for (String key : arrayMap.keySet()) {
            List<?> array = arrayMap.get(key);
            if (array != null) {
                sb.append(key).append(":").append(array);
            }
        }
        return sb.toString();
    }

    public long getLong(String key) {
        return get(key, Long.class);
    }

    public String toJsonString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (String key : classMap.keySet()) {
            sb.append("\"");
            sb.append(key);
            sb.append("\":");
            sb.append(valueString(classMap.get(key)));
            sb.append(",");
        }
        //TODO remove last?

        for (String k : arrayMap.keySet()) {
            List<?> array = arrayMap.get(k);
            sb.append("\"");
            sb.append(k);
            sb.append("\":");
            sb.append(valueString(array));
        }

        if (sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("}");
        return sb.toString();
    }

    public String valueString(List<?> array) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (Object o : array) {
            sb.append(valueString(o));
            sb.append(",");
        }
        if (sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append("]");
        return sb.toString();
    }

    private String valueString(Object obj) {
        if (obj == null) {
            return "null";
        }
        if (obj instanceof JSONModel) {
            JSONModel m = (JSONModel) obj;
            return m.toJsonString();
        }

        if (obj instanceof String) {
            String s = (String) obj;
            return JSONObject.quote(s);
        }
        return obj.toString();
    }

    public Map<String, Object> getAll() {
        HashMap<String, Object> all = new HashMap<>();
        for (String k : classMap.keySet()) {
            Object obj = classMap.get(k);
            if (obj instanceof JSONModel) {
                JSONModel value = (JSONModel) obj;
                all.put(k, value.getAll());
            } else {
                all.put(k, obj);
            }
        }
        return all;
    }
}
