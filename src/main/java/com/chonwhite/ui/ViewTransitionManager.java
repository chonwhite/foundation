package com.chonwhite.ui;

import android.content.Intent;
import android.support.v4.app.Fragment;

public interface ViewTransitionManager {
    void pushFragment(Class<? extends Fragment> fragmentClass, Intent data);
    void pushViewController(ViewController vc);

    void popViewController(ViewController vc);
}
