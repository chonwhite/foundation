package com.chonwhite.ui;

import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.TextView;

import com.chonwhite.json.JSONModel;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;

import liqp.Template;
import liqp.filters.Filter;


public class Binder {

    SparseArray<View> views = new SparseArray<>();
    Field[] fields;

    private void bindFields(Object obj, View view) {
        fields = obj.getClass().getDeclaredFields();
        for (Field f : fields) {
            Binding b = f.getAnnotation(Binding.class);
            if (b != null) {
                View v = null;
                if (b.id() != -1) {
                    v = view.findViewById(b.id());
                } else {
                    if (!TextUtils.isEmpty(b.tag())) {
                        v = view.findViewWithTag(b.tag());
                    }
                }

                if (v != null) {
                    views.append(b.id(), v);
                    try {
                        f.setAccessible(true);
                        f.set(obj, v);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        Log.e("xx", "field:" + f);
                        Log.e("xx", "view:" + v);
                    }
                }
            }
            OnClick onClick = f.getAnnotation(OnClick.class);
            if (onClick != null) {
                View v = null;
                if (onClick.id() != -1) {
                    v = view.findViewById(onClick.id());
                } else if (!TextUtils.isEmpty(onClick.tag())) {
                    v = view.findViewWithTag(onClick.tag());
                }
                if (v != null) {
                    try {
                        boolean isAccessible = f.isAccessible();
                        f.setAccessible(true);
                        v.setOnClickListener((View.OnClickListener) f.get(obj));
                        f.setAccessible(isAccessible);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }

    private void bindMethods(Object obj, View view) {
        Method[] methods = obj.getClass().getDeclaredMethods();
        for (Method m : methods) {

        }
    }

    public void bindView(Object obj, View view) {
        bindFields(obj, view);
        bindMethods(obj, view);
    }

    private static DecimalFormat df = new DecimalFormat("###0.00");

    private void bindTextView(TextView tv, JSONModel model, Binding b) {
        try{
            Template template = Template.parse(b.format());
            String text = template.render(model.getAll());
            tv.setText(text);
        }catch(Exception e){
            e.printStackTrace();
            Log.e("xx","format:" + b.format());
        }
    }

    private static DecimalFormat moneyFormat = new DecimalFormat("###0.00");
    static {
        Filter.registerFilter(new Filter("money") {
            @Override
            public Object apply(Object value, Object... objects) {
                double amount;
                if (value == null) {
                    amount = 0.00;
                } else {
                    if (value instanceof Double) {
                        amount = (Double) value;
                    } else if (value instanceof Integer) {
                        amount = (Integer) value;
                    } else if (value instanceof Float) {
                        amount = (Float) value;
                    } else {
                        String text = super.asString(value);
                        try {
                            amount = Double.parseDouble(text);
                        } catch (NumberFormatException e) {
                            amount = 0.00;
                        }
                    }
                }
                return moneyFormat.format(amount);
            }
        });
    }

    public void bindData(JSONModel model) {
        for (Field f : fields) {
            Binding b = f.getAnnotation(Binding.class);
            if (b != null) {
                View v = views.get(b.id());
                if (v != null && !TextUtils.isEmpty(b.format())) {
                    if (v instanceof TextView) {
                        bindTextView((TextView) v, model, b);
                    }
                }
            }
        }
    }
}
