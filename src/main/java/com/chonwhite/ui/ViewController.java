package com.chonwhite.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ViewController {
    private View mView;
    private int layoutId = -1;
    private Fragment mFragment;
    private FragmentActivity mActivity;

    public void setActivity(FragmentActivity a){
        this.mActivity = a;
    }

    public FragmentActivity getActivity(){
        if(mActivity != null){
            return this.mActivity;
        }
        if(mFragment != null){
            return mFragment.getActivity();
        }
        return null;
    }

    public ViewTransitionManager getViewTransitionManager() {
        return viewTransitionManager;
    }

    {
        Layout layout = getClass().getAnnotation(Layout.class);
        if(layout != null){
            layoutId = layout.id();
        }
    }

    protected int getLayoutId(){
        return layoutId;
    }


    public void setViewTransitionManager(ViewTransitionManager viewTransitionManager) {
        this.viewTransitionManager = viewTransitionManager;
    }

    public void setLayoutId(int id){
        this.layoutId = id;
    };

    private ViewTransitionManager viewTransitionManager;

    public View getView(){
        return mView;
    }

    public void onCreateView(LayoutInflater inflater,ViewGroup container){
        if(layoutId != -1){
            if(getFragment() != null) {
                inflater = getFragment().getLayoutInflater(getFragment().getArguments());
            }
            mView = inflater.inflate(layoutId,null,false);
        }
        onConfigureView();
    }

    public void destroyView(){
        onDestroyView();
        mView = null;
    }

    protected void setView(View view){
        this.mView = view;
    }

    //callbacks

    protected void onConfigureView(){}

    protected void onDestroyView(){}

    protected void onResume(){}

    protected void onPause(){}

    protected Fragment getFragment(){
        return mFragment;
    }

    public void setFragment(Fragment f){
        this.mFragment = f;
    }
}
