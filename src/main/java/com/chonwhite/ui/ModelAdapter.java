package com.chonwhite.ui;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.chonwhite.core.ContextProvider;
import com.chonwhite.http.ListResult;
import com.chonwhite.ponny.R;

import java.util.ArrayList;

@SuppressLint("InflateParams")
public abstract class ModelAdapter<T> extends BaseAdapter {

	public static final int ITEM_TYPE_CELL = 0;
	public static final int ITEM_LOAD_MORE = 1;

	public interface PageControl {
		void onLoadPage(int page);
	}

	public interface ViewHolder<T> {
		void setModel(T model);
		View getView();
	}

	@Override
	public boolean isEnabled(int position) {
		return getItemViewType(position) == 0;
	}

	public void setPageControl(PageControl pc) {
		this.pageControl = pc;
	}

	private PageControl pageControl;
	private ListResult<T> listResult;
	private ArrayList<T> models;

	public void setData(ListResult<T> data) {
		this.listResult = data;
		if (models == null) {
			if (data != null) {
				models = data.getModels();// TODO
			}
		} else {
			if (data != null && data.getModels() != null) {
				for(T m:data.getModels()){
					if(!models.contains(m)){
						models.add(m);
					}
				}
			}
		}
		loading = false;
		notifyDataSetChanged();
	}
	
	public ListResult<T> getData(){
		return this.listResult;
	}

	public void setModels(ArrayList<T> models) {
		this.models = models;
		notifyDataSetChanged();
	}
	
	public ArrayList<T> getModels(){
		return this.models;
	}

	public void clear() {
		if (models != null) {
//			models.clear();
			models = null;
		}
		
		listResult = null;
		notifyDataSetChanged();
	}

	private boolean loading = false;

	@Override
	public int getCount() {
		if (listResult == null) {
			if (models != null) {
				return models.size();
			}
			return 0;
		}
		
		if(models == null){
			return 0;
		}

		if (listResult.hasMorePage()) {
			return models.size() + 1;
		}
		return models.size();
	}

	@Override
	public T getItem(int position) {
		if(models != null) {
			return models.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		if (models == null  || position < models.size()) {
			return ITEM_TYPE_CELL;// TODO
		}
		return 1;// TODO 1
	}

	@SuppressWarnings("unchecked")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder<T> holder;
		if (getItemViewType(position) == ITEM_TYPE_CELL) {
			if (convertView == null) {
				holder = onCreateViewHolder(LayoutInflater.from(ContextProvider
						.getContext()));
				convertView = holder.getView();
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder<T>) convertView.getTag();
			}
			holder.setModel(getItem(position));
		} else {
			if (pageControl != null && !loading) {
				pageControl.onLoadPage(listResult.getNextPage());
			}
			Log.e("xx", "load more");
			convertView = LayoutInflater.from(ContextProvider.getContext())
					.inflate(R.layout.item_loading_footer, null);
		}

		return convertView;
	}

	protected abstract ViewHolder<T> onCreateViewHolder(LayoutInflater inflater);
}
