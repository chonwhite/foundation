package com.chonwhite.ui;

import android.support.annotation.StringRes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ActionBar {
    String title() default "";
    @StringRes int titleRes() default -1;
    boolean hidden() default false;
    int navigationIcon() default -10;
    boolean showNavigationIcon() default true;

    int rightBarItemIcon() default 0;
    String rightBarItemText() default "";
}

