package com.chonwhite.ui;

import android.app.Activity;
import android.view.View;

public abstract class ActionBarController {
    abstract public int getLayoutId();
    public abstract void configure(ActionBar ab,View view);
    public abstract void setNavigationButtonListener(View.OnClickListener onClickListener);

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    private Activity activity;

}
