package com.chonwhite.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;

import com.chonwhite.ponny.R;

public class FragmentContainerActivity extends FragmentActivity {

	public static final String KEY_FRAGMENT_CLASS = "key.FragmentClass";
	public interface FragmentContainerCallback{
		Class<? extends BaseFragment> onFirstActivityCreated(FragmentContainerActivity a);
	}
	
	public static void setFragmentContainerCallback(FragmentContainerCallback c){
		sCallback = c;
	}
	private static FragmentContainerCallback sCallback;
	private BaseFragment mFragment;
	private Statistics mStatistics;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		@SuppressWarnings("unchecked")
		Class<? extends Fragment> fragmentClass = (Class<Fragment>) getIntent().getSerializableExtra(KEY_FRAGMENT_CLASS);
		if(fragmentClass == null){
			if(sCallback != null){
				fragmentClass = sCallback.onFirstActivityCreated(this);
			}
		}

		ActivityEventListener bf = null;
		try {
			mFragment = (BaseFragment) fragmentClass.newInstance();
			mStatistics = fragmentClass.getAnnotation(Statistics.class);
			bf = (ActivityEventListener) mFragment;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}catch(NullPointerException e){
            e.printStackTrace();
        }
		bf.onActivityCreated(this);
		bf.willSetContentView(this);
		setContentView(R.layout.activity_fragment_container);
		bf.didSetContentView(this);
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mFragment).commit();
		if(mStatistics != null){
//			MobclickAgent.onPageStart(mStatistics.page());
			Log.e("xx", "onPageStart:" + mStatistics.page());
		}
	}
	
	@Override
	protected void onDestroy() {
		getSupportFragmentManager().beginTransaction().remove(mFragment).commitAllowingStateLoss();
		mFragment.onActivityDestroyed(this);
		mFragment = null;
		if(mStatistics != null){
//			MobclickAgent.onPageEnd(mStatistics.page());
			Log.e("xx", "onPageEnd:" + mStatistics.page());
		}
		super.onDestroy();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if(mFragment != null && mFragment instanceof BaseFragment){
				BaseFragment bf = (BaseFragment) mFragment;
				boolean result = bf.handleBackKey();
				if(!result){
					finish();
				}
				return result;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		Toast.makeText(this,"result:" + requestCode,Toast.LENGTH_LONG).show();
//		super.onActivityResult(requestCode, resultCode, data);
		mFragment.onActivityResult(requestCode,resultCode,data);
	}
}
