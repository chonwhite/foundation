package com.chonwhite.ui;

import android.app.Activity;
import android.content.Intent;

public interface ActivityEventListener {
	void onActivityCreated(Activity a);
	void willSetContentView(Activity a);
	void didSetContentView(Activity a);

	void onActivityDestroyed(Activity a);

	void onActivityResult(Activity a,int requestCode,int resultCode,Intent data);
}
