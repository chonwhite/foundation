package com.chonwhite.http.volley;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.chonwhite.core.ContextProvider;
import com.chonwhite.http.HttpError;
import com.chonwhite.http.HttpRequest;
import com.chonwhite.http.OnPreDispatchListener;
import com.chonwhite.http.RequestDispatcher;
import com.chonwhite.util.SingletonFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VolleyDispatcher extends RequestDispatcher {
    private RequestQueue requestQueue;
    private ExecutorService mThreadPool = Executors.newCachedThreadPool();
    private OnPreDispatchListener mOnPreDispatchListener;

    public OnPreDispatchListener getOnPredispatchListener() {
        return mOnPreDispatchListener;
    }

    public void setOnPredispatchListener(
            OnPreDispatchListener mOnPredispatchListener) {
        this.mOnPreDispatchListener = mOnPredispatchListener;
    }

    public static VolleyDispatcher getInstance() {
        return SingletonFactory.getInstance(VolleyDispatcher.class);
    }

    public VolleyDispatcher() {
        requestQueue = Volley.newRequestQueue(ContextProvider.getContext(),new MHurlStack());
        requestQueue.start();
    }

    private Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    public <T> void dispatch(final HttpRequest<T> req) {
        if (mOnPreDispatchListener != null) {
            mOnPreDispatchListener.onPreDispatchRequest(req);
        }
        String url = req.getUrl();
        Log.e("xx", "volley url===:" + url);
        StringRequest stringRequest = new StringRequest(req.getMethod(), url,
                new Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        mThreadPool.submit(new Runnable() {
                            @SuppressWarnings("unchecked")
                            @Override
                            public void run() {
                                HttpRequest.Parser<T, String> parser = (HttpRequest.Parser<T, String>) req
                                        .getParser();
                                try {
                                    final T result = parser.parse(response);
                                    mHandler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                req.getListener().onResult(req,
                                                        result);
                                            } catch (Exception e) {
                                                //TODO
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(error.networkResponse != null) {
                    String msg = new String(error.networkResponse.data);
                    Log.e("xxe", "msg" + msg);
                }
                if (req.getErrorListener() != null) {
                    HttpError err = new HttpError();
                    if(error.networkResponse != null){
                        err.setStatusCode(error.networkResponse.statusCode);
                        err.setData(error.networkResponse.data);
                    }

                    err.setErrorMsg(error.getMessage());
                    if (error instanceof TimeoutError) {
                        err.setErrorMsg("网络超时！");
                    } else if (error instanceof NoConnectionError) {
                        err.setErrorMsg("连接服务器失败，稍候重试！");
                    } else if (error instanceof ServerError) {
                        err.setErrorMsg("服务器异常！");
                    }
                    // err.setStatusCode(error.get)
                    err.setCause(error.getCause());
                    req.getErrorListener().onError(req, err);
                }
                Log.e("xx", "volley error:" + error);
                Log.e("xx", "volley url:" + req.getUrl());
                Log.e("xx", "volley url:" + error.getMessage());

            }
        });
        stringRequest.setParams(req.getParams());
        stringRequest.setBody(req.getBody());
        stringRequest.setShouldCache(false);
        if (req.getMethod() == HttpRequest.POST) {
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        }
        requestQueue.add(stringRequest);
    }
}
