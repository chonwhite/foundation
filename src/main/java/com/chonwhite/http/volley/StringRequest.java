package com.chonwhite.http.volley;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.chonwhite.http.CookieManager;
import com.chonwhite.http.CookieModel;
import com.chonwhite.http.HttpBody;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringRequest extends Request<String> {

	private Map<String, String> params = null;

    private HttpBody body;

    public void setBody(HttpBody body){
        this.body =body;
    }
	
	private Listener<String> responseListener;
	public StringRequest(int method, String url,Listener<String> responseListener, ErrorListener listener) {
		super(method, url, listener);
		this.responseListener = responseListener;
	}

	@Override
	protected Response<String> parseNetworkResponse(NetworkResponse response) {
		Log.e("xx", "response headers:" + response.headers);
        if(response.headers != null){

            ArrayList<String> cookies = new ArrayList<>();
            for(String k:response.headers.keySet()){
                if(k.toLowerCase().contains("set-cookie")){
                    cookies.add(response.headers.get(k));
                }
            }

            for(String cookie:cookies){
                String expires = null;
                String maxAge = null;
                String path = null;
                String[] components = cookie.split(";");
                ArrayList<CookieModel> cookieList = new ArrayList<>();
                for(String s:components){
                    String[] values = s.trim().split("=");
                    if(values.length == 2){
                        String key = values[0];
                        if(key.toLowerCase().equals("expires")){
                            expires = values[1];
                            continue;
                        }
                        if(key.toLowerCase().equals("max-age")){
                            maxAge = values[1];
                            continue;
                        }
                        if(key.toLowerCase().equals("path")){
                            path = values[1];
                            continue;
                        }
                        CookieModel cookieModel = new CookieModel();
                        cookieModel.setExpires(expires);
                        cookieModel.setName(key);
                        cookieModel.setDomain(getDomain());
                        cookieModel.setValue(values[1]);

                        Log.e("xx", "key:" + key);
                        Log.e("xx","value:" + values[1]);
                        cookieList.add(cookieModel);
                    }
                }
                CookieManager.getInstance().save(cookieList);
                Log.e("xx","cookie:" + cookies);
            }
        }

		return Response.success(new String(response.data), HttpHeaderParser.parseCacheHeaders(response));
	}

    private String getDomain(){
        URI uri = URI.create(getUrl());
        return uri.getHost();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        super.getHeaders();
        HashMap<String,String> headers = new HashMap<>();
        List<CookieModel> cookies =  CookieManager.getInstance().getCookiesForDomain(getDomain());
        StringBuilder sb = new StringBuilder();
        for(CookieModel cookie:cookies){
            sb.append(cookie.getName()).append("=").append(cookie.getValue());
            sb.append(";");
        }
        headers.put("Cookie",sb.toString());
        headers.put("Content-Type","application/json");//TODO
        Log.e("xx", "cookies:" + headers);
        return headers;
    }

    @Override
	protected void deliverResponse(String response) {
		responseListener.onResponse(response);
	}
	
	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		Log.e("xx","params" + params);
		return params;
	}

	public void setParams(Map<String, String> params){
		this.params = params;
	}

    @Override
    public byte[] getBody() throws AuthFailureError {
        if(body != null){
            Log.e("xx","body:" + Arrays.toString(body.getBody()));
            return body.getBody();
        }
        Log.e("xx","body super");
        return super.getBody();
    }

    @Override
    public String getBodyContentType() {
        return "application/json";
    }
}
