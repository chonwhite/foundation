package com.chonwhite.http;

public interface HttpBody {
    byte[] getBody();
}
