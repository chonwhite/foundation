package com.chonwhite.http;

import android.util.Log;

public class StringBody implements HttpBody {

    private String body;

    public void setBody(String s){
        this.body = s;
    }

    @Override
    public byte[] getBody() {
        Log.e("xx", "body:" + body);
        return body.getBytes();
    }
}
