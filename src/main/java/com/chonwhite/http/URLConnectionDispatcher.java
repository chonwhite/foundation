package com.chonwhite.http;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.chonwhite.util.SingletonFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class URLConnectionDispatcher  extends RequestDispatcher{

    public static URLConnectionDispatcher getInstance(){
        return SingletonFactory.getInstance(URLConnectionDispatcher.class);
    }

    private ExecutorService service = Executors.newCachedThreadPool();
    private Handler handler = new Handler(Looper.getMainLooper());

    @Override
    public <T> void dispatch(final HttpRequest<T> r) {
        service.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    ConnectionWrapper wrapper = new ConnectionWrapper(r.getUrl());
                    for(String key:r.getParams().keySet()){
                        String value = r.getParams().get(key);
                        wrapper.addFormField(key,value);
                    }
                    for(String key:r.getFiles().keySet()){
                        File file = r.getFiles().get(key);
                        wrapper.addFilePart(key, file);
                    }

                    StringBuilder sb = new StringBuilder();
                    List<String> response;
                    try {
                        response = wrapper.finish();
                        for (String line : response) {
                            Log.e("xx","line:" + line);
                            sb.append(line);
                            sb.append("\n");
                        }
                        @SuppressWarnings("unchecked")
                        HttpRequest.Parser<T, String> parser = (HttpRequest.Parser<T, String>) r.getParser();
                        final T result = parser.parse(sb.toString());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                r.getListener().onResult(r,result);
                            }
                        });
                    } catch (final HttpError httpError) {
                        httpError.printStackTrace();
                        if(r.getErrorListener() != null){
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    r.getErrorListener().onError(r,httpError);
                                }
                            });
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    final HttpError error = new HttpError();
                    error.setCause(e);
                    if(r.getErrorListener() != null){
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                r.getErrorListener().onError(r,error);
                            }
                        });
                    }
                }
            }
        });
    }

    private static class ConnectionWrapper{

        private String getDomain(){
            URI uri = URI.create(url);
            return uri.getHost();
        }

        private static final String LINE_FEED = "\r\n";

        private String url;
        private String boundary;
        private String charset = "UTF-8";
        private PrintWriter writer;
        private OutputStream outputStream;
        private HttpURLConnection connection;

        ConnectionWrapper(String urlString) throws IOException {
            this.url = urlString;
//            this.boundary = "UCD_CW_BD_" + UUID.randomUUID() + "_UCD_CW_BD_END";
            boundary = "===" + System.currentTimeMillis() + "===";
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDoOutput(true);//TODO
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data; boundary=" + boundary);
            connection.setRequestProperty("User-Agent", "CodeJava Agent");

            List<CookieModel> cookies =  CookieManager.getInstance().getCookiesForDomain(getDomain());
            StringBuilder sb = new StringBuilder();
            for(CookieModel cookie:cookies){
                sb.append(cookie.getName());
                sb.append("=");
                sb.append(cookie.getValue());
                sb.append(";");
            }
            connection.setRequestProperty("Cookie",sb.toString());
            outputStream = connection.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset),
                    true);
        }

        /**
         * Adds a form field to the request
         * @param name field name
         * @param value field value
         */
        public void addFormField(String name, String value) {
            writer.append("--").append(boundary).append(LINE_FEED);
            writer.append("Content-Disposition: form-data; name=\"" + name + "\"")
                    .append(LINE_FEED);
            writer.append("Content-Type: text/plain; charset=" + charset).append(
                    LINE_FEED);
            writer.append(LINE_FEED);
            writer.append(value).append(LINE_FEED);
            writer.flush();
        }

        /**
         * Adds a upload file section to the request
         * @param fieldName name attribute in <input type="file" name="..." />
         * @param uploadFile a File to be uploaded
         * @throws IOException
         */
        public void addFilePart(String fieldName, File uploadFile)
                throws IOException {
            String fileName = uploadFile.getName();
//            Toast.makeText(ContextProvider.getContext(),uploadFile.getName(),Toast.LENGTH_LONG).show();
            writer.append("--").append(boundary).append(LINE_FEED);
            writer.append(
                    "Content-Disposition: form-data; name=\"" + fieldName
                            + "\"; filename=\"" + fileName + "\"")
                    .append(LINE_FEED);
            writer.append(
                    "Content-Type: "
                            + URLConnection.guessContentTypeFromName(fileName))
                    .append(LINE_FEED);
            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
            writer.append(LINE_FEED);
            writer.flush();

            FileInputStream inputStream = new FileInputStream(uploadFile);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
                Log.e("xx","written:" + bytesRead);
            }
            outputStream.flush();
            inputStream.close();

            writer.append(LINE_FEED);
            writer.flush();
        }

        /**
         * Adds a header field to the request.
         * @param name - name of the header field
         * @param value - value of the header field
         */
        public void addHeaderField(String name, String value) {
            writer.append(name + ": " + value).append(LINE_FEED);
            writer.flush();
        }

        public List<String> finish() throws HttpError {
            List<String> response = new ArrayList<>();

            writer.append(LINE_FEED).flush();
            writer.append("--" + boundary + "--").append(LINE_FEED);
            writer.close();

            // checks server's status code first
            int status = -1;
            try {
                status = connection.getResponseCode();
                if (status == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(
                            connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        response.add(line);
                    }
                    reader.close();
                    connection.disconnect();
                } else {
                    HttpError error = new HttpError();
                    error.setStatusCode(status);
                    throw error;
                }
            } catch (IOException e) {
                HttpError error = new HttpError();
                error.setStatusCode(status);
                error.setCause(e);
                throw error;
            }
            return response;
        }
    }
}
