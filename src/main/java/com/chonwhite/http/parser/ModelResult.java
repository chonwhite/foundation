package com.chonwhite.http.parser;

import com.chonwhite.http.HttpResult;

public class ModelResult<T> extends HttpResult<T> {

	private T model;

	public void setModel(T m) {
		this.model = m;
	}

	public T getModel() {
		return model;
	}

	public boolean isSuccess() {
		return code >= 200 && code < 300;
	}

	private int code;
	private String desc;

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public void setCode(int code) {
		this.code = code;
	}

	@Override
	public String getDesc() {
		return desc;
	}

	@Override
	public void setDesc(String desc) {
		this.desc = desc;
	}

}
