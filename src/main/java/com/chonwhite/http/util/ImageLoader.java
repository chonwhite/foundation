package com.chonwhite.http.util;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.LruCache;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.ImageLoader.ImageListener;
import com.android.volley.toolbox.Volley;
import com.chonwhite.core.ContextProvider;

public class ImageLoader {
	
	public static void load(String url,ImageView imageView){
//		load(url,imageView,R.drawable.img_loading, R.drawable.img_loading);
	}
	
	public static void load(String url,ImageView imageView,int loadingRes,int failureRes){
		if(TextUtils.isEmpty(url)){
            imageView.setImageResource(failureRes);
			return;
		}
		ImageListener listener = com.android.volley.toolbox.ImageLoader.getImageListener(imageView, loadingRes, failureRes);    
		mImageLoader.get(url, listener);
	}
	
	static RequestQueue mQueue = Volley.newRequestQueue(ContextProvider.getContext());
	static com.android.volley.toolbox.ImageLoader mImageLoader = new com.android.volley.toolbox.ImageLoader(
			mQueue, new BitmapCache());

	public static class BitmapCache implements ImageCache {
		private LruCache<String, Bitmap> mCache;

		public BitmapCache() {
			int maxSize = 10 * 1024 * 1024;
			mCache = new LruCache<String, Bitmap>(maxSize) {
				@Override
				protected int sizeOf(String key, Bitmap value) {
					return value.getRowBytes() * value.getHeight();
				}
			};
		}

		@Override
		public Bitmap getBitmap(String url) {
			return mCache.get(url);
		}

		@Override
		public void putBitmap(String url, Bitmap bitmap) {
			mCache.put(url, bitmap);
		}

	}
}
