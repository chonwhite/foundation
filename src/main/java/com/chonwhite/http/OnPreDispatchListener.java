package com.chonwhite.http;

public interface OnPreDispatchListener {
	void onPreDispatchRequest(HttpRequest<?> req);
}
