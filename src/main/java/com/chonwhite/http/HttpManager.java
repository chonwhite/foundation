package com.chonwhite.http;


import com.chonwhite.core.ModelManager;
import com.chonwhite.util.SingletonFactory;

public class HttpManager extends ModelManager<HttpEvent, HttpResult<?>> {
	public static HttpManager getInstance(){
		return SingletonFactory.getInstance(HttpManager.class);
	}
}
