package com.chonwhite.http;

import com.chonwhite.core.ModelManager;
import com.chonwhite.util.SingletonFactory;

import java.util.Collection;
import java.util.List;

public class CookieManager extends ModelManager<String,CookieModel> {

    public static CookieManager getInstance(){
        return SingletonFactory.getInstance(CookieManager.class);
    }

	public void save(CookieModel cookieModel){
        CookieDao.getInstance().save(cookieModel);
    }

    public void save(Collection<CookieModel> cookies){
        CookieDao.getInstance().save(cookies);
    }

    public List<CookieModel> getCookiesForDomain(String domain){
        return CookieDao.getInstance().getCookiesForDomain(domain);
    }
}
