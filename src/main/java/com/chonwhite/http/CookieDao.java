package com.chonwhite.http;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import com.chonwhite.core.BaseDao;
import com.chonwhite.util.SingletonFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CookieDao extends BaseDao<CookieModel> {

    private static final int VERSION = 1;

    public static CookieDao getInstance() {
        return SingletonFactory.getInstance(CookieDao.class);
    }


    /**
     * 根据名称获取cookie
     */
    public CookieModel getByName(String name) {
        CookieModel cookie = null;
        Cursor cursor = getDb().query("cookie_table",
                new String[]{"id", "name", "value", "expires", "domain"}, "name =?",
                new String[]{name}, null, null, null, null);
        if (cursor == null) {
            return null;
        }
        while (cursor.moveToNext()) {
            cookie = cursorToCookieModel(cursor);
            break;
        }
        cursor.close();
        return cookie;

    }

    @Override
    protected Pair<String, String> getPrimaryKeyAndValue(CookieModel cookieModel) {
        String value = cookieModel.getName();
        return new Pair<>(cookieModel.getName(), value);
    }

    @Override
    public boolean contains(CookieModel cookieModel) {
        boolean contains = false;
        Cursor cursor = getDb().query("cookie_table",
                new String[]{"id", "name", "value", "expires", "domain"}, "[name] =?",
                new String[]{cookieModel.getName()}, null, null, null, null);
        if (cursor != null) {
            contains = (cursor.moveToNext());
            cursor.close();
        }
        return contains;
    }




    @Override
    protected String getTableName() {
        return "cookie_table";
    }

    @Override
    protected long insert(CookieModel cookieModel) {
        return super.insert(cookieModel);
    }

    @Override
    protected void fillContentValues(ContentValues cv, CookieModel cookie) {
        super.fillContentValues(cv, cookie);
        cv.put("value", cookie.getValue());
        cv.put("expires", cookie.getExpires());
        cv.put("domain", cookie.getDomain());
        cv.put("name", cookie.getName());
    }

    @Override
    protected void fillFields(CookieModel cookie, Cursor cursor) {
        super.fillFields(cookie, cursor);
        cookie.setId(cursor.getLong(cursor.getColumnIndex("id")));
        cookie.setName(cursor.getString(cursor.getColumnIndex("name")));
        cookie.setValue(cursor.getString(cursor.getColumnIndex("value")));
        cookie.setExpires(cursor.getString(cursor.getColumnIndex("expires")));
        cookie.setDomain(cursor.getString(cursor.getColumnIndex("domain")));
    }

    @Override
    protected String getDatabaseFileName() {
        return "cookie.db";
    }

    //---------------old
    /**
     * 更新cookie
     */
    public boolean update(CookieModel cookie) {
        ContentValues values = new ContentValues();
        fillContentValues(values,
                cookie);
        getDb().update("cookie_table", values, "[name] =  ?",
                new String[]{cookie.getName()});
        return true;
    }

    // 删除cookie
    public boolean delete(CookieModel cookie) {
        String name = cookie.getName();
        delete(name);
        return true;
    }

    public void delete(String name) {
        getDb().delete("cookie_table", "[name]= ?", new String[]{name});
    }

    /**
     * 清空
     */
    public void clear() {
        getDb().delete("cookie_table", null, null);
    }

    /**
     * 列出没有过期的cookie
     *
     * @return
     */
    public ArrayList<CookieModel> listAvailable() {
        ArrayList<CookieModel> cookies = new ArrayList<CookieModel>();

        String clientTime = "" + (new Date()).getTime();
        Cursor cursor = getDb().query("cookie_table",
                new String[]{"id", "name", "value", "expires", "domain"},
                "expires>=?", new String[]{clientTime}, null, null, null, null);
        if (cursor == null) {
            return cookies;
        }
        while (cursor.moveToNext()) {
            CookieModel cookie = cursorToCookieModel(cursor);
            cookies.add(cookie);
        }

        cursor.close();

        return cookies;
    }

    public ArrayList<CookieModel> listar() {
        ArrayList<CookieModel> cookies = new ArrayList<CookieModel>();

        Cursor cursor = getDb().query("cookie_table",
                new String[]{"id", "name", "value", "expires", "domain"}, null, null,
                null, null, null, null);

        if (cursor == null) {
            return cookies;
        }

        while (cursor.moveToNext()) {
            CookieModel cookie = cursorToCookieModel(cursor);
            cookies.add(cookie);
        }

        cursor.close();

        return cookies;
    }

    private CookieModel cursorToCookieModel(Cursor cursor) {
        CookieModel cookie = new CookieModel();
        if (cursor == null) {
            return cookie;
        }

        return cookie;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS [cookie_table] " +
                "([id] INTEGER primary key autoincrement, " + "[name] TEXT," + // cookie名
                "[value] TEXT," + // cookie值
                "[expires] TEXT, " +
                "[domain] TEXT);"// 过期时间
        );
    }

    public List<CookieModel> getCookiesForDomain(String domain) {
        ArrayList<CookieModel> cookies = new ArrayList<>();
        Cursor cursor = getDb().rawQuery("SELECT * FROM [cookie_table] WHERE [domain] = \"" + domain + "\"",null);
        while (cursor.moveToNext()){
            CookieModel cookie = new CookieModel();
            fillFields(cookie,cursor);
            cookies.add(cookie);
        }
        if(cursor != null){
            cursor.close();
        }
        return cookies;
    }
}
