package com.chonwhite.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.chonwhite.ui.DimensionUtil;


public class BadgeView extends View {

	public BadgeView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public BadgeView(Context context) {
		super(context);
	}

	private String mText = "0";
	private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	private boolean disableBackgroundIfZero = true;
	
	{
		mPaint.setColor(Color.WHITE);
		if(isInEditMode()){
			mPaint.setTextSize(20);
		}else{
			mPaint.setTextSize(DimensionUtil.dip2px(10));
		}
		
	}
	
	public void setText(String text){
		int originalLenth = mText == null ? 0: mText.length();
		int newLength = text == null ? 0:text.length();
		if(originalLenth != newLength){
			requestLayout();
		}
		mText = text;
		if(text != null){
			mPaint.getTextBounds(mText, 0, mText.length(), textBounds);
		}
		invalidate();
	}

	public void setTextSize(float size){
		mPaint.setTextSize(size);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		if(mText != null && !isInEditMode()){
//			int width = (int) mPaint.measureText(mText);
//			int height =mPaint.getF
			
			
			int width = (int) (textBounds.width() * 1.4);
			int height = (int) (textBounds.height() * 1.4);
			
			
			int minSize = DimensionUtil.dip2px(16);
			width = Math.max(width, minSize);
			height = Math.max(height, minSize);
			
			if(width > height && width <= height * 1.4){
				height = width;
			}
			if(height > width){
				width = height;
			}
			
//			if(MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY){
//				height = MeasureSpec.getSize(heightMeasureSpec);
//			}
			
			setMeasuredDimension(width,height);
		}else{
			setMeasuredDimension(0, 0);
		}
	}
	
	RectF bgBound = new RectF();
	Rect bounds = new Rect();
	Rect textBounds = new Rect();
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(TextUtils.isEmpty(mText)){
			return;
		}
		canvas.getClipBounds(bounds);
		if(disableBackgroundIfZero && "0".equals(mText)){
			
		}else{
			
			bgBound.set(bounds);
			mPaint.setColor(Color.RED);
			canvas.drawOval(bgBound, mPaint);
		}
		

		mPaint.setColor(Color.WHITE);
		
		int x = (bounds.width() - textBounds.width()) / 2;
		int y = getHeight() - (bounds.height() - textBounds.height()) / 2 - 2 ;
		
		canvas.drawText(mText, x, y , mPaint);
	}

}
