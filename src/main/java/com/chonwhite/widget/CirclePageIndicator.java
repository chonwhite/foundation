package com.chonwhite.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;

import com.chonwhite.ui.DimensionUtil;


public class CirclePageIndicator extends View {
	public interface OnPageItemClickListener{
		void onPageItemClicked(int position);
	}

	private ViewPager viewPager;
	private OnPageItemClickListener pageItemClickListener;
	
	public void setOnPageItemClickListener(OnPageItemClickListener itemClickListener){
		pageItemClickListener = itemClickListener;
	}
	
	private OnPageChangeListener pageChangeListener = new OnPageChangeListener() {
		@Override
		public void onPageSelected(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg1, float xx, int arg3) {
			total = viewPager.getAdapter().getCount();
			current = viewPager.getCurrentItem();
			invalidate();
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}
	};

	private int total;
	private int current;
	private int radius = 30;
	private int unselectedColor = Color.GRAY;
	private int selectedColor = Color.WHITE;
	private OnGestureListener mGestureListener = new OnGestureListener() {
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			Log.e("xx","clicked:" + current);
			if(pageItemClickListener != null){
				pageItemClickListener.onPageItemClicked(current);
			}
			return true;
		}
		
		@Override
		public void onShowPress(MotionEvent e) {
			
		}
		
		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
				float distanceY) {
			return false;
		}
		
		@Override
		public void onLongPress(MotionEvent e) {
		}
		
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			return false;
		}
		
		@Override
		public boolean onDown(MotionEvent e) {
			return false;
		}
	};

	public void setViewPager(final ViewPager viewPager) {
		this.viewPager = viewPager;
		viewPager.removeOnPageChangeListener(pageChangeListener);
		viewPager.addOnPageChangeListener(pageChangeListener);
		radius = DimensionUtil.dip2px(4);
		startAnimation();
		mGestureDetector = new GestureDetector(getContext(), mGestureListener);
	}

	public CirclePageIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CirclePageIndicator(Context context) {
		super(context);
	}

	Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
	{
		mPaint.setTextSize(50);
		mPaint.setColor(Color.RED);
	}

	Handler mHandler = new Handler();
	private int interval = 3000;
	private boolean intercepting;
	private long lastTouchEvent = 0;
	
	private Runnable swipeRun = new Runnable() {
		@Override
		public void run() {
			if(viewPager == null){
				return;
			}
			mHandler.postDelayed(swipeRun, interval);
			if(intercepting){
				return;
			}
			if(System.currentTimeMillis() - lastTouchEvent < interval){
				return;
			}
			int target = current + 1;
			if(current >= total - 1){
				target = 0;
			}
			viewPager.setCurrentItem(target, true);
		}
	};
	
	
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		startAnimation();
	}
	
	public void startAnimation(){
		mHandler.removeCallbacks(swipeRun);
		mHandler.postDelayed(swipeRun, interval);
	}
	
	public void stopAnimation(){
		mHandler.removeCallbacks(swipeRun);
	}
	
	
	GestureDetector mGestureDetector;
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(viewPager !=null){
			viewPager.onTouchEvent(event);
		}
		mGestureDetector.onTouchEvent(event);
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			intercepting = true;
			break;
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_CANCEL:
			intercepting = false;
			lastTouchEvent = System.currentTimeMillis();
			break;
		default:
			break;
		}
		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(viewPager.getAdapter() != null){
			total = viewPager.getAdapter().getCount();
		}
		int x = (int) (getWidth() - (total * 1.5) * radius * 2);
		int y = (int) (getHeight() - getPaddingBottom() - radius * 1.5);
		for (int i = 0; i < total; i++) {
			if (i == current) {
				mPaint.setColor(selectedColor);
			} else {
				mPaint.setColor(unselectedColor);
			}
			canvas.drawCircle(x, y, radius, mPaint);
			x += radius * 3;
		}
	}
}
